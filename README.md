# Physically based cloud rendering
Bachelor of Science Thesis at the Faculty of Electrical Engineering and Computing, University of Zagreb, under the mentorship of prof. dr. sc. Željka Mihajlović.

## Abstract
This bachelor’s thesis presents physically based interaction of light with particles inside a volume. Two systems for quantifying light are introduced – radiometry and photometry. Physical features of clouds and their effect on the appearance of clouds is presented. A cloud model based on noise functions is introduced. Ray tracing is presented as a method of rendering volumetric objects. Methods for approximating ambient illumination and multiple scattering inside a cloud are given. Some optimization methods are introduced with the goal of reducing rendering times. Results of the thesis are presented and evaluated. 

Keywords: physically based shading, volume rendering, physics of light, noise functions, ray tracing

## Examples
![stratocumulus](/paper-images/readme/stratocumulus_cropped.png)
![cumulus](/paper-images/readme/c_multiple_ambient_cropped.png)
![sunset](/paper-images/readme/sunset_cropped.png)

